<?php

namespace App\GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Http;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ProductsResolver
{
    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $response = Http::get('https://fakestoreapi.com/products', [
            'limit' => 25,
        ]);


        return $this->paginate($args['page'], $args['count'], $response->json());

    }


    private function paginate(int $page, int $limit, array $items): array
    {
        $data = [];

        foreach ($items as $index => $item) {
            if ($index > ($page - 1) * $limit && count($data) < $limit) {
                $item['name'] = $item['title'];
                $data[] = $item;
            }
        }

        return $data;
    }
}
